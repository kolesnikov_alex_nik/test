from rest_framework import serializers
from .models import Visit, SalePoint


class VisitSerialiser(serializers.ModelSerializer):

    salePoint = serializers.PrimaryKeyRelatedField(many=False, queryset=SalePoint.objects.only('id'))
    class Meta:
        model = Visit
        fields = ('date', 'salePoint', 'lon', 'lat')


class SalePointSerializer(serializers.ModelSerializer):

    class Meta:
        model = SalePoint
        fields = ('id', 'name')
