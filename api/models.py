from django.db import models


class Emploee(models.Model):
    name = models.CharField('имя', max_length=100)
    phone = models.CharField('номер телефона', max_length=20)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Работник"
        verbose_name_plural = "Работники"


class SalePoint(models.Model):
    name = models.CharField('имя', max_length=255)
    emploee = models.ForeignKey(Emploee, verbose_name="Работник", related_name="emploee_sale_point", on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Торговая точка"
        verbose_name_plural = "Торговые точки"


class Visit(models.Model):
    date = models.DateTimeField()
    salePoint = models.ForeignKey(SalePoint, verbose_name="Торговая точка", related_name="visit_sale_point", on_delete=models.CASCADE)
    lon = models.FloatField()
    lat = models.FloatField()

    def __str__(self):
        return f'{self.date} - {self.salePoint.name}'

    class Meta:
        verbose_name = "Посещение"
        verbose_name_plural = "Посещения"
