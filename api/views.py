from rest_framework import viewsets
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from .models import Visit, SalePoint
from .serializers import VisitSerialiser, SalePointSerializer


class VisitApiView(CreateAPIView):
    serializer_class = VisitSerialiser
    queryset = Visit.objects.all()


class SalePointApiView(viewsets.ReadOnlyModelViewSet):
    queryset = None

    def list(self, request, *args, **kwargs):
        phone = self.request.query_params.get('phone')

        if phone is not None:
            self.queryset = SalePoint.objects.filter(emploee__phone=phone).values('id', 'name')
        else:
            self.queryset = SalePoint.objects.values('id', 'name')
        serializer = SalePointSerializer(self.queryset, many=True)
        return Response(serializer.data)
