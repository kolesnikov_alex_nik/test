from django.contrib import admin

from api.models import Emploee, SalePoint, Visit


@admin.register(Emploee)
class EmploeeAdmin(admin.ModelAdmin):
    pass


@admin.register(SalePoint)
class SalePointAdmin(admin.ModelAdmin):
    pass


@admin.register(Visit)
class VisitAdmin(admin.ModelAdmin):
    readonly_fields = []

    def get_readonly_fields(self, request, obj=None):
        return list(self.readonly_fields) + \
               [field.name for field in obj._meta.fields] + \
               [field.name for field in obj._meta.many_to_many]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False